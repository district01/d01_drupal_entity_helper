# d01_drupal_entity_helper #

Provides helper functions to fetch entity field values.

# Usage #

Call an entity helper service: 
```php 
$general_helper = EntityHelper::generalHelper() 
``` 

Call one of the helper functions: 
```php 
$value = EntityHelper::generalHelper()->getValue($entity, $field_name);
$values = EntityHelper::generalHelper()->getValues($entity, $field_name);
$field_item_list = EntityHelper::generalHelper()->getFieldItemList($entity, $field_name);
``` 

# Helper types #

| Type | Call | 
| ---------------- | ------------- |
| General | `EntityHelper::generalHelper()` |
| Processed text | `EntityHelper::processedTextHelper()` |
| Entity reference | `EntityHelper::entityReferenceHelper()` |
| Image | `EntityHelper::imageHelper()` |
| File | `EntityHelper::fileHelper()` |
| Date | `EntityHelper::dateHelper()` |
| Date range | `EntityHelper::dateRangeHelper()` |
| Link | `EntityHelper::linkHelper()` |


# Release notes #

`1.0`
* First implementation of helper

`2.0`
* Change getUrl() method to load image over http/https

`3.0`
* Improve documentation
* Added an entity helper service for links

`4.0`
* Fixes in date helper to use site's timezone
* Provide a readme with code examples
* Added checks for non-existing/empty field item lists

`5.0`
* Remove unnecessary imports
* Fix referenced items returning NULL if the item that is referenced 
is deleted but the entity containing the reference is not yet updated. 

`6.0`
* Added new method getAbsoluteUrl() to get the full url of an image.
* File entity helper getUrl() now **requires** a FileInterface object as first parameter

`7.0`
* Added static methods to fetch entity helper services.
* Deprecated the 'getType' function

`7.1.0`
* Added a Boolean helper.
* Added a Number helper.
* Added a Range helper.
* Updated Date helper to work on fields that don't have a computed date property (ex : created, published, ...).

`7.1.1`
* Added getFormatStringPeriods helper function to the DateInterval helper.

`7.1.2`
* Drupal 9 compatibility.