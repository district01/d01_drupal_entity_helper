<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class EntityFieldTypeHelperBoolean.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperBoolean extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperGeneralInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $item = $item_list->first();
    if (!$item) {
      return FALSE;
    }

    return $item->value ? (bool) $item->value : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      $values[] = $item->value ? (bool) $item->value : FALSE;
    }

    return $values;
  }

}
