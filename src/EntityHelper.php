<?php

namespace Drupal\d01_drupal_entity_helper;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityHelper.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityHelper implements EntityHelperInterface {

  /**
   * The fallback entity field type helper.
   *
   * @var EntityFieldTypeHelperFallback
   */
  protected $entityFieldTypeHelperFallback;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityFieldTypeHelperFallback $entity_field_type_helper_fallback) {
    $this->entityFieldTypeHelperFallback = $entity_field_type_helper_fallback;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('d01_drupal_entity_helper.field_type_helper_fallback')
    );
  }

  /**
   * Retrieves the container.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface
   *   The currently active global container.
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Retrieves the general helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperGeneral
   *   The general helper.
   */
  public static function generalHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_general');
  }

  /**
   * Retrieves the date helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperDate
   *   The date helper.
   */
  public static function dateHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_date');
  }

  /**
   * Retrieves the date interval helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperDateInterval
   *   The date interval helper.
   */
  public static function dateIntervalHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_date_interval');
  }

  /**
   * Retrieves the date range helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperDateRange
   *   The date range helper.
   */
  public static function dateRangeHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_date_range');
  }

  /**
   * Retrieves the entity reference helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperEntityReference
   *   The entity reference helper.
   */
  public static function entityReferenceHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_entity_reference');
  }

  /**
   * Retrieves the file helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperFile
   *   The file helper.
   */
  public static function fileHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_file');
  }

  /**
   * Retrieves the image helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperImage
   *   The image helper.
   */
  public static function imageHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_image');
  }

  /**
   * Retrieves the link helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperLink
   *   The link helper.
   */
  public static function linkHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_link');
  }

  /**
   * Retrieves the processed text helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperProcessedText
   *   The processed text helper.
   */
  public static function processedTextHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_processed_text');
  }

  /**
   * Retrieves the number helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperNumber
   *   The number helper.
   */
  public static function numberHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_number');
  }

  /**
   * Retrieves the boolean helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperBoolean
   *   The boolean helper.
   */
  public static function booleanHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_boolean');
  }

  /**
   * Retrieves the range helper.
   *
   * @return \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperRange
   *   The range helper.
   */
  public static function rangeHelper() {
    return static::getContainer()->get('d01_drupal_entity_helper.field_type_helper_range');
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated in entity helper version 7.0. Will be removed in version 9.0.
   *   Use static methods to fetch helpers instead.
   */
  public function setType($type) {
    $service = $this->buildServiceName($type);
    if (\Drupal::hasService($service)) {
      return \Drupal::service($service);
    }

    return $this->entityFieldTypeHelperFallback;
  }

  /**
   * Build the service name.
   *
   * @param string $type
   *   The entity helper type.
   *
   * @return string
   *   The built service name.
   *
   * @deprecated in entity helper version 7.0. Will be removed in version 9.0.
   *   Use static methods to fetch helpers instead.
   */
  protected function buildServiceName($type) {
    return 'd01_drupal_entity_helper.field_type_helper_' . $type;
  }
}
