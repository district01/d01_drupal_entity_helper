<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class EntityFieldTypeHelperFallback.
 *
 * This class is used as a fallback class. It will only be used when a
 * non-existing service is called from the EntityHelper class.
 *
 * @package Drupal\d01_drupal_entity_helper
 *
 * @deprecated in entity helper version 7.0. Will be removed in version 9.0.
 *   This class is not needed anymore since 'magic' dependency injection in the
 *   EntityHelper class - using the 'setType' function - will be removed.
 */
class EntityFieldTypeHelperFallback implements EntityFieldTypeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    return FALSE;
  }
}
