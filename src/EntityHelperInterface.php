<?php

namespace Drupal\d01_drupal_entity_helper;

/**
 * Interface EntityHelperInterface.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
interface EntityHelperInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\d01_drupal_entity_helper\EntityFieldTypeHelperFallback $entity_field_type_helper_fallback
   *   The entity type manager.
   */
  public function __construct(EntityFieldTypeHelperFallback $entity_field_type_helper_fallback);

  /**
   * Set the entity helper type.
   *
   * @param string $type
   *   An entity helper service type.
   *
   * @deprecated in entity helper version 7.0. Will be removed in version 9.0.
   *   Use static methods to fetch helpers instead.
   */
  public function setType($type);
}
