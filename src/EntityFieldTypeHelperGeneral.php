<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class EntityFieldTypeHelperGeneral.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperGeneralInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $item = $item_list->first();
    if (!$item) {
      return FALSE;
    }

    return $item->value ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      $values[] = $item->value;
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldItemList(ContentEntityInterface $entity, $field) {
    if (!$entity->hasField($field)) {
      return FALSE;
    }

    $item_list = $entity->get($field);
    if ($item_list->isEmpty()) {
      return FALSE;
    }

    return $item_list;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldConfig(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return $entity->getFieldDefinition($field);
    }

    return $item_list->getFieldDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldLabel(ContentEntityInterface $entity, $field) {
    $fieldConfig = $this->getFieldConfig($entity, $field);

    if (!$fieldConfig) {
      return FALSE;
    }

    return $fieldConfig->getLabel();
  }
}
