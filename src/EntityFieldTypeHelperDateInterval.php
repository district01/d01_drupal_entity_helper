<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Component\Utility\Unicode;

/**
 * Class EntityFieldTypeHelperDateInterval.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperDateInterval extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    if (!$entity->hasField($field)) {
      return FALSE;
    }

    try {
      return new \DateInterval($entity->{$field}->value);
    } catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      if (!$item) {
        continue;
      }

      $values[] = $this->getValue($entity, $field);
    }

    return $values;
  }

  /**
   * Return a format to pass through DateInterval, taking plurality and zeroes
   * into account.
   *
   * If we just print an interval using \DateInterval->format, we don't always
   * know beforehand which format to pass exactly, to make sure it's properly
   * presented.
   * When using '%d days %h hours %i minutes', we prefer not to get this :
   * - 1 days 0 hours 4 minutes
   * but instead, we prefer to get something like this (notice plural/zeroes) :
   * - 1 day 4 minutes
   *
   * @param \DateInterval $dateInterval
   *    The DateInterval to be checked.
   * @param bool $zeroes
   *    Whether or not to show periods with a value of 0.
   * @param bool $years
   *    Whether or not to include years in the final format.
   * @param bool $months
   *    Whether or not to include months in the final format.
   * @param bool $days
   *    Whether or not to include days in the final format.
   * @param bool $hours
   *    Whether or not to include hours in the final format.
   * @param bool $minutes
   *    Whether or not to include minutes in the final format.
   * @param bool $seconds
   *    Whether or not to include seconds in the final format.
   *
   * @return string
   */
  public function getFormatStringPeriods(\DateInterval $dateInterval, $zeroes = FALSE, $years = FALSE, $months = FALSE, $days = TRUE, $hours = TRUE, $minutes = TRUE, $seconds = FALSE) {
    $formatStringParts = [];
    $formatPeriods = [
      '%y year(s)' => $years,
      '%m month(s)' => $months,
      '%d day(s)' => $days,
      '%h hour(s)' => $hours,
      '%i minute(s)' => $minutes,
      '%s second(s)' => $seconds,
    ];

    foreach ($formatPeriods as $formatPeriodString => $enabled) {
      if ($enabled) {
        $numeric = intval($dateInterval->format($formatPeriodString));

        if ($zeroes || (!$zeroes && $numeric !== 0)) {
          // Split into prefix, actual param & suffix.
          $prefix = mb_substr($formatPeriodString, 0, mb_strpos($formatPeriodString, '%'));
          $format = mb_substr($formatPeriodString, mb_strpos($formatPeriodString, '%'), 2);
          $suffix = mb_substr($formatPeriodString, mb_strpos($formatPeriodString, $format) + mb_strlen($format));

          $open = preg_quote('(', '/');
          $close = preg_quote(')', '/');
          $pattern = "/$open(.*)$close/";

          // Replace the plural from the format-string, if the value of the
          // period is just 1.
          list ($prefix, $suffix) = preg_replace($pattern, $numeric == 1 ? '' : '$1', array ($prefix, $suffix));

          // Reconstruct the format, which now has the correct plurality.
          $formatStringParts[] = $prefix.$format.$suffix;
        }
      }
    }

    return implode(' ', $formatStringParts);
  }
}
