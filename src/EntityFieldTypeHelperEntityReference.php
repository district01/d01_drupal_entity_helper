<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class EntityFieldTypeHelperReference.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperEntityReference extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $item = $item_list->first();
    if (!$item) {
      return FALSE;
    }

    return $item->entity ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {

      // Referenced items can return NULL if the item
      // that is referenced is deleted but the entity containing
      // the reference is not yet updated. So we need to check
      // if the entity exists.
      if (!$item->entity) {
        continue;
      }

      $values[] = $item->entity;
    }

    return $values;
  }

}
