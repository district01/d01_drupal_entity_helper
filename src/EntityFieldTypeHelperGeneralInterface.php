<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface EntityFieldTypeHelperGeneralInterface.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
interface EntityFieldTypeHelperGeneralInterface {

  /**
   * Get a single field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field value from.
   * @param string $field
   *   The field name.
   *
   * @return mixed|bool
   *   A single object.
   */
  public function getValue(ContentEntityInterface $entity, $field);

  /**
   * Get multiple field values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field values from.
   * @param string $field
   *   The field name.
   *
   * @return array|bool
   *   An array of objects
   */
  public function getValues(ContentEntityInterface $entity, $field);

  /**
   * Get the field item list.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field values from.
   * @param string $field
   *   The field name.
   *
   * @return mixed|bool
   *   The FieldItemList.
   */
  public function getFieldItemList(ContentEntityInterface $entity, $field);

  /**
   * Get the field configuration.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field configuration from.
   * @param string $field
   *   The field name.
   *
   * @return \Drupal\field\Entity\FieldConfig|bool
   *   The field definition.
   */
  public function getFieldConfig(ContentEntityInterface $entity, $field);

  /**
   * Get the field label.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field label from.
   * @param string $field
   *   The field name.
   *
   * @return string|bool
   *   The field label.
   */
  public function getFieldLabel(ContentEntityInterface $entity, $field);
}
