<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class EntityFieldTypeHelperProcessedText.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperProcessedText extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $item = $item_list->first();
    if (!$item) {
      return FALSE;
    }

    return [
      '#type' => 'processed_text',
      '#text' => $item->value,
      '#format' => $item->format,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      if (!$item) {
        continue;
      }

      $values[] = [
        '#type' => 'processed_text',
        '#text' => $item->value,
        '#format' => $item->format,
      ];
    }

    return $values;
  }

  /**
   * Get a single formatted field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field value from.
   * @param string $field
   *   The field name.
   * @param string $format
   *   The text format to apply.
   *
   * @return array|bool
   *   A renderable array.
   */
  public function getForcedFormattedValue(ContentEntityInterface $entity, $field, $format) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $item = $item_list->first();
    if (!$item) {
      return FALSE;
    }

    return [
      '#type' => 'processed_text',
      '#text' => $item->value,
      '#format' => $format,
    ];
  }

  /**
   * Get multiple formatted field values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field value from.
   * @param string $field
   *   The field name.
   * @param string $format
   *   The text format to apply.
   *
   * @return array|bool
   *   A renderable array.
   */
  public function getForcedFormattedValues(ContentEntityInterface $entity, $field, $format) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      $values[] = [
        '#type' => 'processed_text',
        '#text' => $item->value,
        '#format' => $format,
      ];
    }

    return $values;
  }
}
