<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityFieldTypeHelperDate.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperDate extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(DateFormatter $date_formatter) {
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    if (!$entity->hasField($field)) {
      return FALSE;
    }

    if ($entity->{$field}->date) {
      return $entity->{$field}->date;
    }

    // Some fields like created, published, ... don't have
    // a Computed Date stored in the date property.
    // They just store the timestamp in their value property.
    // When no date property is found we check the value property to
    // upcast that timestamp to a DrupalDateTime object.
    if ($entity->{$field}->value && is_string($entity->{$field}->value)) {
      $date = DrupalDateTime::createFromTimestamp($entity->{$field}->value);
      if (!$date->hasErrors()) {
        return $date;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      if (!$item) {
        continue;
      }

      if ($item->date) {
        $values[] = $item->date;
        continue;
      }

      // Some fields like created, published, ... don't have
      // a Computed Date stored in the date property.
      // They just store the timestamp in their value property.
      // When no date property is found we check the value property to
      // upcast that timestamp to a DrupalDateTime object.
      if ($item->value && is_string($item->value)) {
        $date = DrupalDateTime::createFromTimestamp($item->value);
        if (!$date->hasErrors()) {
          $values[] = $date;
          continue;
        }
      }
    }

    return $values;
  }

  /**
   * Get a single timestamp value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field value from.
   * @param string $field
   *   The field name.
   *
   * @return string|bool
   *   The timestamp.
   */
  public function getTimestampValue(ContentEntityInterface $entity, $field) {
    $date = $this->getValue($entity, $field);
    if (!$date) {
      return FALSE;
    }

    return $date->getTimestamp() ?: FALSE;
  }

  /**
   * Format the specified date.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   The date to format.
   * @param string $format
   *   A valid php date format.
   * @param string $timezone
   *   An optional timezone.
   *
   * @return string|bool
   *   The formatted date.
   */
  public function formatDate(DrupalDateTime $date, $format, $timezone = NULL) {
    if (!$date instanceof DrupalDateTime) {
      return FALSE;
    }

    $timestamp = $date->getTimestamp();

    $timezone = $timezone ?: \Drupal::config('system.date')->get('timezone.default');

    return $this->dateFormatter->format($timestamp, 'custom', $format, $timezone);
  }
}
