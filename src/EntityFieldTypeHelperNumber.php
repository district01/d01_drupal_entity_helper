<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class EntityFieldTypeHelperNumber.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperNumber extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $item = $item_list->first();
    if (!$item) {
      return FALSE;
    }

    $field_type = $item_list->getFieldDefinition()->getType();

    return $item->value ? $this->formatValue($item->value, $field_type) : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $field_type = $item_list->getFieldDefinition()->getType();

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      $formatted_value = $this->formatValue($item->value, $field_type);
      if ($formatted_value) {
        $values[] = $formatted_value;
      }
    }

    return $values;
  }

  /**
   * Return the correctly typed number for the given value.
   *
   * @param mixed $value
   *   The value to format.
   * @param string $field_type
   *   The field type.
   *
   * @return float|int|bool
   *   The type casted result.
   */
  private function formatValue($value, $field_type) {
    switch ($field_type) {
      case 'decimal':
      case 'float':
        $result = (float) $value;
        break;

      case 'integer':
        $result = (int) $value;
        break;

      default:
        $result = FALSE;
    }

    return $result;
  }
}
