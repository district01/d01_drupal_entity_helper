<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\file\FileInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityFieldTypeHelperFile.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperFile extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    if (!$entity->hasField($field)) {
      return FALSE;
    }

    return $entity->{$field}->entity ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {

      // Referenced items can return NULL if the item
      // that is referenced is deleted but the entity containing
      // the reference is not yet updated. So we need to check
      // if the entity exists.
      if (!$item->entity) {
        continue;
      }

      $values[] = $item->entity;
    }

    return $values;
  }

  /**
   * Get the relative file url for the given file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param string|null $image_style
   *   Optional image style to use.
   *
   * @return string
   *   Relative File Url with the specified settings.
   */
  public function getUrl(FileInterface $file, $image_style = NULL) {
    $absolute_url = $this->getAbsoluteUrl($file, $image_style);
    return $absolute_url ? file_url_transform_relative($absolute_url) : $absolute_url;
  }

  /**
   * Get the absolute file url for the given file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param string|null $image_style
   *   Optional image style to use.
   *
   * @return string
   *   Absolute File Url with the specified settings.
   */
  public function getAbsoluteUrl(FileInterface $file, $image_style = NULL) {
    $uri = $file->getFileUri();
    if (!$uri) {
      return FALSE;
    }

    if ($image_style) {
      $style = $this->entityTypeManager->getStorage('image_style')
        ->load($image_style);
      return $style ? $style->buildUrl($uri) : FALSE;
    }

    return file_create_url($uri);
  }

}
