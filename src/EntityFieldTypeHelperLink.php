<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Url;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Class EntityFieldTypeHelperLink.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperLink extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    return $item_list->first() ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {

      if (!$item) {
        continue;
      }

      $values[] = $item;
    }

    return $values;
  }

  /**
   * Get the title for the given link.
   *
   * @param \Drupal\link\Plugin\Field\FieldType\LinkItem $link
   *   A link object.
   *
   * @return string|bool
   *   The passed link's title.
   */
  public function getTitle(LinkItem $link) {
    return $link->title ?: FALSE;
  }

  /**
   * Get the url for the given link.
   *
   * @param \Drupal\link\Plugin\Field\FieldType\LinkItem $link
   *   A link object.
   *
   * @return \Drupal\Core\Url|bool
   *   The passed link's url.
   */
  public function getUrl(LinkItem $link) {
    $uri = $link->uri;
    if (!$uri) {
      return FALSE;
    }

    try {
      $url = Url::fromUri($uri);
    }
    catch (\Exception $exception) {
      $url = FALSE;
    }

    return $url;
  }

  /**
   * Determine the target for the given link.
   *
   * @param \Drupal\link\Plugin\Field\FieldType\LinkItem $link
   *   A link object.
   *
   * @return string|bool
   *   The target attribute value.
   */
  public function getTarget(LinkItem $link) {
    return $link->isExternal() ? '_blank' : '_self';
  }
}
