<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\range\RangeItemInterface;

/**
 * Class EntityFieldTypeHelperRange.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
class EntityFieldTypeHelperRange extends EntityFieldTypeHelperGeneral implements EntityFieldTypeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $item = $item_list->first();
    if (!$item) {
      return FALSE;
    }

    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(ContentEntityInterface $entity, $field) {
    $item_list = $this->getFieldItemList($entity, $field);
    if (!$item_list) {
      return FALSE;
    }

    $values = [];
    foreach ($item_list->getIterator() as $item) {
      $values[] = $item;
    }

    return $values;
  }

  /**
   * Get the 'min' value from a given Range.
   *
   * @param \Drupal\range\RangeItemInterface $range
   *    A Range object.
   *
   * @return mixed
   *    The min value of the Range.
   */
  public function getMin(RangeItemInterface $range) {
    return $range->from;
  }

  /**
   * Get the 'max' value from a given Range.
   *
   * @param \Drupal\range\RangeItemInterface $range
   *    A Range object.
   *
   * @return mixed
   *    The max value of the Range.
   */
  public function getMax(RangeItemInterface $range) {
    return $range->to;
  }

  /**
   * Get the combined 'min' & 'max' values from a given Range.
   *
   * @param \Drupal\range\RangeItemInterface $range
   *    A Range object.
   * @param string $glue
   *    The glue to combine the min & max values.
   *
   * @return string
   *    The min & max values concatenated by the glue.
   */
  public function getMinMax(RangeItemInterface $range, $glue = ' - ') {
    return implode($glue, [
      $this->getMin($range),
      $this->getMax($range),
    ]);
  }
}
