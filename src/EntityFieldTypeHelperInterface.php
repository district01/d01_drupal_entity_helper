<?php

namespace Drupal\d01_drupal_entity_helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface EntityFieldTypeHelperInterface.
 *
 * @package Drupal\d01_drupal_entity_helper
 */
interface EntityFieldTypeHelperInterface {

  /**
   * Get a single field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field value from.
   * @param string $field
   *   The field name.
   */
  public function getValue(ContentEntityInterface $entity, $field);

  /**
   * Get multiple field values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field values from.
   * @param string $field
   *   The field name.
   */
  public function getValues(ContentEntityInterface $entity, $field);
}
